/// <reference types="react" />
declare const _default: ({
    id: string;
    type: string;
    data: {
        label: JSX.Element;
    };
    position: {
        x: number;
        y: number;
    };
    style?: undefined;
    source?: undefined;
    target?: undefined;
    label?: undefined;
    animated?: undefined;
    arrowHeadType?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    data: {
        label: JSX.Element;
    };
    position: {
        x: number;
        y: number;
    };
    type?: undefined;
    style?: undefined;
    source?: undefined;
    target?: undefined;
    label?: undefined;
    animated?: undefined;
    arrowHeadType?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    data: {
        label: JSX.Element;
    };
    position: {
        x: number;
        y: number;
    };
    style: {
        background: string;
        color: string;
        border: string;
        width: number;
        stroke?: undefined;
    };
    type?: undefined;
    source?: undefined;
    target?: undefined;
    label?: undefined;
    animated?: undefined;
    arrowHeadType?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    position: {
        x: number;
        y: number;
    };
    data: {
        label: string;
    };
    type?: undefined;
    style?: undefined;
    source?: undefined;
    target?: undefined;
    label?: undefined;
    animated?: undefined;
    arrowHeadType?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    type: string;
    data: {
        label: string;
    };
    position: {
        x: number;
        y: number;
    };
    style?: undefined;
    source?: undefined;
    target?: undefined;
    label?: undefined;
    animated?: undefined;
    arrowHeadType?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    source: string;
    target: string;
    label: string;
    type?: undefined;
    data?: undefined;
    position?: undefined;
    style?: undefined;
    animated?: undefined;
    arrowHeadType?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    source: string;
    target: string;
    type?: undefined;
    data?: undefined;
    position?: undefined;
    style?: undefined;
    label?: undefined;
    animated?: undefined;
    arrowHeadType?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    source: string;
    target: string;
    animated: boolean;
    label: string;
    type?: undefined;
    data?: undefined;
    position?: undefined;
    style?: undefined;
    arrowHeadType?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    source: string;
    target: string;
    arrowHeadType: string;
    label: string;
    type?: undefined;
    data?: undefined;
    position?: undefined;
    style?: undefined;
    animated?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    source: string;
    target: string;
    type: string;
    label: string;
    data?: undefined;
    position?: undefined;
    style?: undefined;
    animated?: undefined;
    arrowHeadType?: undefined;
    labelStyle?: undefined;
} | {
    id: string;
    source: string;
    target: string;
    type: string;
    style: {
        stroke: string;
        background?: undefined;
        color?: undefined;
        border?: undefined;
        width?: undefined;
    };
    label: string;
    animated: boolean;
    labelStyle: {
        fill: string;
        fontWeight: number;
    };
    data?: undefined;
    position?: undefined;
    arrowHeadType?: undefined;
})[];
export default _default;
