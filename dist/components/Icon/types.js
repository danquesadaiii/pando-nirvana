"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IconColorEnum;
(function (IconColorEnum) {
    IconColorEnum[IconColorEnum["primary"] = 0] = "primary";
    IconColorEnum[IconColorEnum["secondary"] = 1] = "secondary";
    IconColorEnum[IconColorEnum["success"] = 2] = "success";
    IconColorEnum[IconColorEnum["warning"] = 3] = "warning";
    IconColorEnum[IconColorEnum["danger"] = 4] = "danger";
    IconColorEnum[IconColorEnum["light"] = 5] = "light";
    IconColorEnum[IconColorEnum["dark"] = 6] = "dark";
})(IconColorEnum || (IconColorEnum = {}));
