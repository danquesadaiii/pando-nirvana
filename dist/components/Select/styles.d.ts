import { ThemeProvider } from '../../themes';
import { SelectProps } from './types';
export declare const StyledSelectContainer: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const StyledField: import("styled-components").StyledComponent<"div", any, SelectProps & ThemeProvider, never>;
