export declare const StyledMenu: import("styled-components").StyledComponent<"div", any, {}, never>;
export interface IconContainerProp {
    align?: string;
}
export interface MenuContent extends IconContainerProp {
    is_open: boolean;
}
export declare const MenuContent: import("styled-components").StyledComponent<"div", any, MenuContent, never>;
export declare const MenuContainer: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const IconContainer: import("styled-components").StyledComponent<"div", any, IconContainerProp, never>;
