import { TableProps } from './types';
export declare const getResponsiveHeaders: ({ columns }: TableProps) => string;
export declare const StyledTable: import("styled-components").StyledComponent<any, any, object, string | number | symbol>;
