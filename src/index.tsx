// import ReactDOM from 'react-dom'

// import { StyledApp } from './styles'

// const App: React.FC = () => (
//   <StyledApp className='App'>
//     <p>Run `yarn storybook` instead.</p>
//   </StyledApp>
// )

// ReactDOM.render(<App />, document.getElementById('root'))

export * from './components'
export * from './applications'
export * from './themes'
