import React from 'react'

import { DraggableProps } from './types'

const Draggable: React.FC<DraggableProps> = () => <div>Draggable</div>

export default Draggable
