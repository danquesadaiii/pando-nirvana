export interface DraggableProps {
  draggableId?: string
  type: string
}
