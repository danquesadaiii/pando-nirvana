import React from 'react'

import { DroppableProps } from './types'

const Droppable: React.FC<DroppableProps> = () => <div>Droppable</div>

export default Droppable
