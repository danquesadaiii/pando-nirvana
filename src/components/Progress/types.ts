export interface ProgressProps {
  /**
   * Progress percentage
   */
  progress: number
  /**
   * Progress percentage
   */
  color?:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'warning'
    | 'danger'
    | 'light'
    | 'dark'
}
