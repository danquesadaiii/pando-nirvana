export interface LogoProps {
  src: string
  alt: string
  orientation?: 'horizontal' | 'vertical'
  companyName?: string
  tagLine?: string
}
