import { baseTheme } from './baseTheme'
import { darkModeTheme } from './darkTheme'

export const Themes = {
  base: baseTheme,
  darkMode: darkModeTheme,
}

// export type ThemesDefinition = typeof Themes

export * from './types'
