import { ThemeDefinition } from './types'

export const baseTheme: ThemeDefinition = {
  bgColors: {
    card: '#f2f2f2',
    container: '#ffffff',
  },
  border: {
    width: 0,
    radius: '6px',
  },
  badge: {
    xs: {
      fontSize: 13,
      fontWeight: 300,
      padding: '4px 8px',
    },
    sm: {
      fontSize: 14,
      fontWeight: 300,
      padding: '5px 9px',
    },
    md: {
      fontSize: 15,
      fontWeight: 300,
      padding: '6px 10px',
    },
    lg: {
      fontSize: 16,
      fontWeight: 300,
      padding: '7px 11px',
    },
    xl: {
      fontSize: 17,
      fontWeight: 300,
      padding: '8px 12px',
    },
  },
  button: {
    xs: {
      fontSize: 13,
      fontWeight: 300,
      padding: '8px 18px',
    },
    sm: {
      fontSize: 14,
      fontWeight: 300,
      padding: '9px 19px',
    },
    md: {
      fontSize: 15,
      fontWeight: 300,
      padding: '10px 20px',
    },
    lg: {
      fontSize: 16,
      fontWeight: 300,
      padding: '10px 20px',
    },
    xl: {
      fontSize: 17,
      fontWeight: 300,
      padding: '10px 20px',
    },
  },
  colors: {
    primary: {
      50: '#EFF6FF',
      100: '#DBEAFE',
      light: '#96CACD',
      DEFAULT: '#2C969B',
      dark: '#006267',
      // light: '#90CAF9',
      // DEFAULT: '#2C969B',
      // dark: '#356AC3',
    },
    secondary: {
      50: '#F3F4F6',
      100: '#E5E7Eb',
      light: '#80F2CE',
      DEFAULT: '#00E59E',
      dark: '#00C17A',
      // light: '#D1D5DB',
      // DEFAULT: '#6B7280',
      // dark: '#374152',
    },
    success: {
      50: '#ECFDF5',
      100: '#D1FAE5',
      light: '#C3EEDF',
      DEFAULT: '#10B981',
      dark: '#87DCC0',
      // light: '#6EE7B7',
      // DEFAULT: '#10B981',
      // dark: '#047857',
    },
    warning: {
      50: '#FFFBEB',
      100: '#FEF3C7',
      light: '#FCD34D',
      DEFAULT: '#F59E0B',
      dark: '#B45309',
    },
    danger: {
      50: '#FEF2F2',
      100: '#FEE2E2',
      light: '#FBD0D0',
      DEFAULT: '#EF4444',
      dark: '#F7A1A1',
      // light: '#FCA5A5',
      // DEFAULT: '#DC2626',
      // dark: '#B91C1C',
    },
    light: {
      light: '#ffffff',
      DEFAULT: '#eeeeee',
      dark: '#dddddd',
    },
    dark: {
      light: '#344054',
      DEFAULT: '#1D2939',
      dark: '#101828',
    },
  },
  headers: {
    h1: {
      fontSize: 2,
      fontWeight: 700,
      padding: '5px 10px',
    },
    h2: {
      fontSize: 1.5,
      fontWeight: 700,
      padding: '5px 10px',
    },
    h3: {
      fontSize: 1,
      fontWeight: 700,
      padding: '5px 10px',
    },
    h4: {
      fontSize: 0.875,
      fontWeight: 500,
      padding: '5px 10px',
    },
    h5: {
      fontSize: 0.75,
      fontWeight: 500,
      padding: '5px 10px',
    },
    h6: {
      fontSize: 0.7,
      fontWeight: 500,
      padding: '5px 10px',
    },
    // todo
  },
  icon: {
    xs: {
      height: 18,
    },
    sm: {
      height: 21,
    },
    md: {
      height: 25,
    },
    lg: {
      height: 27,
    },
    xl: {
      height: 30,
    },
  },
  input: {
    border: 0,
  },
  list: {
    padding: 0,
    listStyle: 'none',
  },
  loader: {
    xs: {
      fontSize: 36,
      inButtonSize: 13,
    },
    sm: {
      fontSize: 38,
      inButtonSize: 14,
    },
    md: {
      fontSize: 40,
      inButtonSize: 15,
    },
    lg: {
      fontSize: 42,
      inButtonSize: 16,
    },
    xl: {
      fontSize: 44,
      inButtonSize: 17,
    },
  },
  table: {},
  text: {
    fontUrl:
      'https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap',
    fontFamily: 'Poppins, sans-serif',
    size: {
      xs: {
        fontSize: 10,
        fontWeight: 400,
      },
      sm: {
        fontSize: 12,
        fontWeight: 400,
      },
      md: {
        fontSize: 14,
        fontWeight: 400,
      },
      lg: {
        fontSize: 18,
        fontWeight: 400,
      },
      xl: {
        fontSize: 20,
        fontWeight: 400,
      },
    },
  },
  size: {
    // master gene, todo discuss if ok ra
    xs: '320px',
    sm: '480px',
    md: '768px',
    lg: '1024px',
    xl: '1200px',
    // xs: '16.66666666666667%',
    // sm: '33.33333333333333%',
    // md: '50%',
    // lg: '66.66666666666667%',
    // xl: '83.33333333333334%',
  },
}
