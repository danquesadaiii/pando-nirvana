import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { Form } from '../components'

export default {
  title: 'Components/Form',
  component: Form,
} as ComponentMeta<typeof Form>

const Template: ComponentStory<typeof Form> = (args) => {
  const fields = {
    test: {
      label: 'test',
      required: true,
      place_holder: 'enter test',
      name: 'test',
      field_type: 'text',
      icon: 'search',
    },
  }
  const someSideEffect = <T,>(data: T) => {
    console.log('Side Effects:', data)
  }

  return (
    <Form
      {...args}
      title='THIS IS A TEST FORM'
      onSubmit={someSideEffect}
      fields={fields}
    />
  )
}

export const Default = Template.bind({})
